

### DESARROLLO DE UNA APLICACIÓN MÓVIL PARA GEOLOCALIZACIÓN DE MOTOTAXIS Y USUARIOS EN ATESCATEMPA, JUTIAPA. ###

El presente repositorio aloja el documento teórico para el proyecto de graduación con el título que arriba se describe.


### Repositorios de Backend (Spring boot, Hibernate, JPA, Mysql) y Aplicación móvil (Ionic - Angular) ###

* [apis-mototaxi-spring](https://s-valladares@bitbucket.org/s-valladares/apis-mototaxi-spring.git)
* [app-mototaxi-ionic](https://s-valladares@bitbucket.org/s-valladares/app-mototaxi-ionic.git)